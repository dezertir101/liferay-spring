package com.meera.liferay.spring.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderResponse;

import com.meera.liferay.spring.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.util.ParamUtil;
import com.meera.liferay.spring.domain.Contact;
import com.meera.liferay.spring.service.ContactService;
import com.liferay.portal.kernel.messaging.MessageBusUtil;
import com.liferay.portal.kernel.messaging.Message;


@Controller("contactController")
@RequestMapping("VIEW")
public class ContactController    {

	@Autowired
	private ContactService contactService;

    @Autowired
    private EventService eventService;
	
	@RenderMapping
	public String listContacts(Map<String, Object> map) {
        List contactList = contactService.listContact();

        System.out.println("Contact size = " +contactList.size());
        if(contactList.size() != 0){
            Contact lastContact = (Contact) contactList.get(contactList.size() -1);
            Message lastContactMessage = new Message();
            lastContactMessage.put(eventService.getLastContactDestinationName(),lastContact);
            eventService.sendMessage(lastContactMessage);
        }
		map.put("contact", new Contact());
		map.put("contactList", contactService.listContact());
		return "contact";
	}

    @RenderMapping(params = "page=editPage")
    public String renderEditPage(RenderResponse response, Map<String, Object> model) {
        return "edit";
    }

    @RenderMapping(params = "page=defaultPage")
    public String defaultPage(Map<String, Object> map) {
        map.put("contact", new Contact());
        map.put("contactList", contactService.listContact());
        return "contact";
    }
	
	@ActionMapping(params = "action=add")
	public void addContact(ActionRequest actionRequest,ActionResponse actionResponse, Model model,@ModelAttribute("contact") Contact contact, BindingResult result) throws IOException,
			PortletException {
		System.out.println("result"+ParamUtil.getString(actionRequest,"firstname"));
		System.out.println("contact"+contact.getFirstname());
		contactService.addContact(contact);
		//return "contact";
		
	}
	
	@ActionMapping(params = "action=delete")
	public void deleteContact(@RequestParam("contactId") Integer contactId,ActionRequest actionRequest,ActionResponse actionResponse, Model model) throws IOException,
			PortletException {
		System.out.println("resul344444444444444t");
		contactService.removeContact(contactId);
		//return "contact";
		
	}

    @ActionMapping(params = "action=edit")
    public void editContact(@RequestParam("contactId") Integer contactId,ActionRequest actionRequest,ActionResponse actionResponse, Model model) throws IOException,
            PortletException{
        Contact contact = contactService.getContactById(contactId);
        model.addAttribute("contact",contact);
        actionResponse.setRenderParameter("page","editPage");
    }




    @ActionMapping(params = "action=update")
    public void updateContact(ActionRequest actionRequest,ActionResponse actionResponse, Model model,@ModelAttribute("contact") Contact contact, BindingResult result) throws IOException,
            PortletException {
        System.out.println("Contact id = " + contact.getId());
        contactService.updateContact(contact);
        actionResponse.setRenderParameter("page","defaultPage");
    }
}
