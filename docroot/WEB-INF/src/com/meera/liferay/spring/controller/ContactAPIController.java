package com.meera.liferay.spring.controller;

        import java.io.IOException;
        import java.util.HashMap;
        import java.util.List;
        import java.util.Map;

        import javax.portlet.ActionRequest;
        import javax.portlet.ActionResponse;
        import javax.portlet.PortletException;
        import javax.portlet.RenderResponse;

        import org.springframework.ui.Model;
        import com.meera.liferay.spring.domain.Contact;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Controller;
        import org.springframework.web.bind.annotation.*;

        import com.liferay.portal.kernel.util.ParamUtil;
        import com.meera.liferay.spring.service.ContactService;
        import org.springframework.web.portlet.ModelAndView;

@Controller
@RequestMapping("/api/contact")
public class ContactAPIController {

    @Autowired
    private ContactService contactService;


    @RequestMapping(value = "/get/{value}", method = RequestMethod.GET )
    public @ResponseBody String getContact(@PathVariable Integer contactId,Model model) {
        Contact contact =  contactService.getContactById(contactId);
        model.addAttribute("contact",contact);
        return "edit";
    }


}
