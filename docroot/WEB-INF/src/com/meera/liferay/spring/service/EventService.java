package com.meera.liferay.spring.service;
import com.liferay.portal.kernel.messaging.MessageBus;
import com.liferay.portal.kernel.messaging.MessageBusUtil;
import com.liferay.portal.kernel.messaging.Message;


public interface EventService {

    public void sendMessage(Message message);

    public String getLastContactDestinationName();
}
