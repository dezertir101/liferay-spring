package com.meera.liferay.spring.service;


import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageBusUtil;
import org.springframework.stereotype.Service;

@Service
public class EventServiceImpl implements EventService{

    protected String lastContactDestinationName = "lastContactDestination";

    public void sendMessage(Message message)
    {
        MessageBusUtil.sendMessage(this.getLastContactDestinationName(),message);
    }

    public String getLastContactDestinationName(){
        return this.lastContactDestinationName;
    }
}
